/**
 * Created by duguying on 2015/10/14.
 */
$(document).ready(function (e) {
    var box = document.getElementById("box");
    var tip = document.getElementById("tip");
    var isExtDown = false;
    box.addEventListener("keydown", function (e) {
        if(event.keyCode == 17 || event.keyCode == 91){
            if(!isExtDown){
                console.log(event.keyCode + " [down]");
                isExtDown = true;
            }
        }else{
            if(isExtDown){
                tip.innerHTML = "ctrl+"+event.keyCode;
            }
        }
    });
    box.addEventListener("keyup", function(e){
        if(event.keyCode == 17 || event.keyCode == 91){
            if(isExtDown){
                console.log(event.keyCode + " [up]");
                isExtDown = false;
            }
        }
    })
});